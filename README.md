# README #

Lea atentamente las siguientes instrucciones, las cuales le proveerán los pasos necesarios 
para descargar, instalar y hacer funcionar el proyecto base de Django
### Pasos Preliminares ###

* Descargue e instale phyton, recomiendo la versión 3,0 en adelante (https://www.python.org/downloads/)
* Cuando insale phyton, si esta utilizando windows al ejecutar, marque el checkbox "add Phyton 3.xx to path"
* Si prefieren, pueden utilizar algun gestor de base de datos para su proyecto, la BD por defecto de Django es Sqlite,
  así que si lo desean pueden descargar un gestor para trabajar la BD desde alli.

### Instalando Django ###

* En su terminal escriba -m pip install django
* Si lo desea, puede configurar un entorno virtual para ejecutar django, el siguiente link provee la manera de como hacerlo
  https://www.youtube.com/watch?v=x82k13-mn-E&list=PLsRdPvQ2xMkH8c2BOnQ_O1KZ9lyyL_eGB&index=2 
   Este video tambien demuestra como instalar django de manera global. Recomiendo el tutorial, ya que cada capitulo 
   enseña de manera clara varais cosas muy utiles para desarrollar en django.
   
### Descargando desde Bitbucket e inicializando el proyecto ###

* En bitbucket descargue el proyecto, (si, este mismo) desde la barra lateral izquierda, en Downloads
* Descomprima el .rar y encontrará una carpeta con el nombre "navegadorcito", en ella habran dos carpetas y 3 archivos.
* En su terminal, asegurese de entrar a la carpeta navegadorcito y ejecute el proyecto, para eso el comando
  $python manage.py runserver o $manage.py runserver
* Si abren el navegador y utilizan el http://localhost:8000/ deberian observar el login del proyecto funcionando.

### Y ahora que? ###

* Django provee un backend administrativo mágico, pero para acceder a el necesitamos tener un super usuario
  para eso, desde la consola utilizamos el comando $manage.py createsuperuser o $phyton manage.py createsuperuser
  una vez creado podemos logearnos en la siguiente direccion: localhost:8000/admin/  ...., juegue.
  
* El super usuario que creó tambien sirve para el login de la aplicación, utilicelo para seguir adelante
  con su proyecto si asi lo desea.